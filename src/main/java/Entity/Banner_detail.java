package Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "banner_detail")
public class Banner_detail {
    @Id
    @Column(name = "banner_detail_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long banner_detail_id;
    @Column(name = "banner_description", nullable = false)
    private String description;
    @Column(name = "banner_created_at", nullable = false)
    private LocalDateTime created_at;
    @Column(name = "banner_image", nullable =false)
    private String banner_image;

    @OneToOne
    @JoinColumn(name = "banner")
    private Banner banner;


}
