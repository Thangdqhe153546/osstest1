package Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Banner")
public class Banner {
    @Id
    @Column(name = "banner")
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Long banner_id;

    @Column(name = "banner_title", nullable = false )
    private String banner_title;

    @OneToOne(mappedBy = "banner")
    private Banner_detail banner_detail;


}
